import UIUtils.CommonlyUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.Set;

/**
 * @ClassName test
 * @Date 2019/5/28 4:58 PM
 * @Author WangXuDong
 **/
public class test {



    public String filePath;


    public String getPath() {

        filePath = this.getClass().getClassLoader().getResource("chromedriver").getPath();

        return filePath;

    }


    @Test
    public void main() {

        System.setProperty("webdriver.chrome.driver",this.getPath());

        WebDriver driver = new ChromeDriver();

        CommonlyUtils commonlyUtils = new CommonlyUtils();

        commonlyUtils.getUrl("http://www.baidu.com/", driver);

        Set<Cookie> cookie = commonlyUtils.getCookie(driver);

        for (Cookie value: cookie ){

            System.out.print(value);
        }


    }
}
