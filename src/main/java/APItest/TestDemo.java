package APItest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName TestDemo
 * @Date 2019/5/28 3:15 PM
 * @Author WangXuDong
 *
 * 入门
 *
 **/
public class TestDemo {


    private WebDriver driver;
    private String baseUrl = "http://www.baidu.com";
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeMethod
    public void setUp() throws Exception {

       String filePath = this.getClass().getClassLoader().getResource("chromedriver").getPath();
        //启动chrome浏览器
        System.setProperty("webdriver.chrome.driver", filePath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();// 最大化浏览器
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);//设置操作超时时长，该设置是全局性的，即所有操作都最长等待30s

    }

    @Test
    /**
     * 搜索selenium
     *
     * */
    public void testLogin() throws Exception {
        driver.get(baseUrl); //打开url
        driver.findElement(By.id("kw")).clear();// 按id找到元素后，清空该元素
        driver.findElement(By.id("kw")).sendKeys("selenium");// 输入selenium
        driver.findElement(By.id("su")).click(); //点击搜索按钮
    }

    @AfterMethod
    public void tearDown() throws Exception {

        driver.quit(); //关闭窗口
        String verificationErrorString = verificationErrors.toString();


        if (!"".equals(verificationErrorString)) {
            Assert.fail(verificationErrorString);
        }
    }


}
