package UIUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Condition;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;


/**
 * @ClassName CommonlyUtils
 * @Date 2019/5/28 4:38 PM
 * @Author WangXuDong
 **/
public class CommonlyUtils {







    /*
     * 获取driver
     * */
    public WebDriver getDriver(WebDriver driver) {
        return driver;
    }

    //关闭浏览器驱动方法
    public void stopDriver(WebDriver driver) {
        System.out.println("Stop Driver!");
        driver.close();
    }


    /*
     * 封装Element方法
     *
     * */
    public WebElement findElement(By by,WebDriver driver) {
        WebElement element=driver.findElement(by);
        return element;
    }

    /*
     * get封装
     * */
    public void getUrl(String url,WebDriver driver) {

        driver.get(url);
    }

    /*
     * 封装click（点击）方法
     * 需要传入一个WebElement类型的元素
     *
     * */
    public void click(WebElement element,WebDriver driver) {
        if(element!=null) {
            element.click();
        }else {
            System.out.println("元素未定位到,定位失败");
        }
    }

    /*
     * 返回
     *
     * */
    public void back(WebDriver driver) {
        driver.navigate().back();
    }

    /*
     * 刷新
     *
     * */
    public void refresh(WebDriver driver) {
        driver.navigate().refresh();;
    }

    /**
     * 屏幕最大化
     *
     * */
    public void getWindowMax(WebDriver driver) {
        driver.manage().window().maximize();
    }
    /*
     * 休眠
     * */
    public void sleep(int num,WebDriver driver) {
        try {
            Thread.sleep(num);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /**
     * 切换alert窗口
     *
     * */
    public void switchAlert(WebDriver driver) {
        driver.switchTo().alert();
    }

    /**
     *
     * 模态框切换
     * */
    public void switchToMode(WebDriver driver) {
        driver.switchTo().activeElement();
    }
    /**
     * actionMoveElement
     * */
    public void action(WebElement element,WebDriver driver){
        Actions action =new Actions(driver);
        action.moveToElement(element).perform();
    }

    /**
     * 获取cookcie
     * @return
     * */
    public Set<Cookie> getCookie(WebDriver driver){
        Set<Cookie> cookies = driver.manage().getCookies();
        return cookies;
    }

    /**
     * 删除cookie
     * */
    public void deleteCookie(WebDriver driver){
        driver.manage().deleteAllCookies();
    }
    /**
     * 设置cookie
     * */
    public void setCookie(Cookie cookie,WebDriver driver){
        driver.manage().addCookie(cookie);
    }
    /**
     * 获取当前系统窗口list
     * */
    public List<String> getWindowsHandles(WebDriver driver){
        Set<String> winHandels = driver.getWindowHandles();
        List<String> handles = new ArrayList<String>(winHandels);
        return handles;
    }

    /*
     * 获取当前窗口
     * **/
    public String getWindowHandle(WebDriver driver){
        return driver.getWindowHandle();
    }

    /**
     * 切换windows窗口
     * */
    public void switchWindows(String name,WebDriver driver){
        driver.switchTo().window(name);
    }
    /**
     * 获取当前url
     * */
    public String getUrl(WebDriver driver){
        return driver.getCurrentUrl();
    }

    /**
     * 获取title
     * */
    public String getTitle(WebDriver driver){
        return driver.getTitle();
    }
    /**
     * 传入参数截图
     * */
    public void takeScreenShot(TakesScreenshot drivername, String path,WebDriver driver) {
        String currentPath = System.getProperty("user.dir"); // get current work
        File scrFile = drivername.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File(currentPath + "\\" + path));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("截图成功");
        }
    }

    /**
     * 封装定位一组elements的方法
     * */
    public List<WebElement> findElements(By by,WebDriver driver){
        return driver.findElements(by);
    }







}
